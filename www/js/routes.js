angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('crowdSourcing', {
    url: '/main_1',
    templateUrl: 'templates/crowdSourcing.html',
    controller: 'crowdSourcingCtrl'
  })

  .state('scanning', {
    url: '/page3',
    templateUrl: 'templates/scanning.html',
    controller: 'scanningCtrl'
  })

  .state('actions', {
    url: '/actions_2',
    templateUrl: 'templates/actions.html',
    controller: 'actionsCtrl'
  })

  .state('compare', {
    url: '/page5_compare',
    templateUrl: 'templates/compare.html',
    controller: 'compareCtrl'
  })

$urlRouterProvider.otherwise('/main_1')



});
