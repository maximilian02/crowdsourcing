angular.module('app.services', [])

.service('apiService', function($http, $q, CommonData) {

    this.getMarkets = function() {
      var deffered = $q.defer();

      $http({
        method: 'GET',
        url: CommonData.hostIp + 'supermarkets'
      }).then(function(res) {
          deffered.resolve(res.data);
      }, function(err) {
          deffered.reject({error: 'ERROR'});
      });

      return deffered.promise;
    };

    this.getProduct = function(upc) {
      var market = CommonData.getMarket();
      var deffered = $q.defer();

      $http({
        method: 'GET',
        url: CommonData.hostIp + 'product/' + upc
      }).then(function(res) {
          deffered.resolve(res.data);
      }, function(err) {
          deffered.reject({error: 'ERROR'});
      });

      return deffered.promise;
    };

    this.getProductsCompare = function(upc) {
      var market = CommonData.getMarket();
      var deffered = $q.defer();

      $http({
        method: 'GET',
        url: CommonData.hostIp + 'prices/' + upc
      }).then(function(res) {
          deffered.resolve(res.data);
      }, function(err) {
          deffered.reject({error: 'ERROR'});
      });

      return deffered.promise;
    };

    this.modifyPrice = function(data) {
      var market = CommonData.getMarket();
      var deffered = $q.defer();

      $http({
        method: 'PUT',
        url: CommonData.hostIp + 'price/' + data.upc + '/' + market.id,
        data: {
          value: data.price
        }
      }).then(function(res) {
          deffered.resolve(res.data);
      }, function(err) {
          deffered.reject({error: 'ERROR'});
      });

      return deffered.promise;
    };

    this.createPrice = function(data) {
      var market = CommonData.getMarket();
      var deffered = $q.defer();

      $http({
        method: 'POST',
        url: CommonData.hostIp + 'price',
        data: {
          upc: data.upc,
          name: data.name,
          uom: data.uom,
          idSupermarket: market.id,
          value: data.price
        }
      }).then(function(res) {
          deffered.resolve(res.data);
      }, function(err) {
          deffered.reject({error: 'ERROR'});
      });

      return deffered.promise;
    };

    this.createSupermarket = function(data) {
      var deffered = $q.defer();

      $http({
        method: 'POST',
        url: CommonData.hostIp + 'supermarket',
        data: {
          name: data.name,
          address: data.address
        }
      }).then(function(res) {
          deffered.resolve(res.data);
      }, function(err) {
          deffered.reject({error: 'ERROR'});
      });

      return deffered.promise;
    };

    this.savePicture = function(data) {
      var deffered = $q.defer();

      $http({
        method: 'POST',
        url: CommonData.hostIp + 'photo/' + data.upc,
        data: data.imageData,
        headers: { 'Content-type': 'text/plain' }
      }).then(function(res) {
          deffered.resolve(res.data);
      }, function(err) {
          deffered.reject({error: 'ERROR'});
      });

      return deffered.promise;
    };

    this.scanBCMock = function() {
      return $q.when({
        format: 'SARASA',
        text: '9789879317679'
      });
    };

})

.factory('CommonData', function() {
    var result = {
      market: {},
      product: {},
      hostIp: 'http://192.168.0.11:8080/'
    };

    result.setMarket = function (market) {
      this.market = market;
    };

    result.getMarket = function (market) {
      return this.market;
    };

    result.setProduct = function (product) {
      this.product = product;
    };

    result.getProduct = function (product) {
      return this.product;
    };

    result.filterMarkets = function(markets) {
      var market = this.market;
      var res = markets.filter(function(m) {
        return m.market_id != market.id;
      });

      return res;
    };

    return result;
});
