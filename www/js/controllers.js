angular.module('app.controllers', ['ionic.native'])

.controller('crowdSourcingCtrl', function ($scope, $state, $stateParams, apiService, CommonData) {

  $scope.markets = [];
  $scope.marketSelected = null;
  $scope.disableStart = true;
  $scope.showNewMarketInputs = false;
  $scope.market = {
    address: '',
    name: ''
  };

  function init() {
    apiService.getMarkets()
      .then(function(res){
        $scope.markets = res;
        $scope.markets.push({
          address: '',
          id: '-1',
          name: 'Create a Market'
        });

        angular.forEach($scope.markets, function(market) {
            market.name = !market.address ? market.name : market.name + ' (' + market.address + ')';
        });

      }, function(err) { console.log(err); });
  }

  init();

  $scope.start = function(marketSelected) {
    if(marketSelected.id != -1) {
        CommonData.setMarket(marketSelected);
    } else {
      apiService.createSupermarket($scope.market)
        .then(function(res) {
          CommonData.setMarket({
            id: res.insertId,
            address: $scope.market.address,
            name: $scope.market.name
          });
          $state.go('actions');
        });
    }

  };

  $scope.optionChange = function(marketSelected, market) {
    $scope.showNewMarketInputs = (marketSelected && marketSelected.id == -1);
    $scope.disableStart = marketSelected && marketSelected.id ? false : true;
  };

})

.controller('actionsCtrl', function($scope, $state, $cordovaBarcodeScanner, $cordovaCamera, CommonData, apiService) {

  var marketSelected = CommonData.getMarket();

  if(!marketSelected.id) {
  	$state.go('crowdSourcing');
  }

  $scope.product = {};
  $scope.addButtonFlag = true;
  $scope.hasImage = true;
  $scope.hostIp = CommonData.hostIp;

  function getProduct(upc) {
    apiService.getProduct(upc)
      .then(function(res) {
        var p = res.length ? res[0] : null;

        if(p) {
          $scope.addButtonFlag = true;
          $scope.hasImage = true;
          $scope.product.upc = p.upc;
          $scope.product.name = p.name;
          $scope.product.uom = p.uom;
          CommonData.setProduct($scope.product);
        } else {
          $scope.addButtonFlag = false;
          $scope.hasImage = false;
        }
      });
  }

  $scope.scanBC = function() {

    $cordovaBarcodeScanner.scan()
    	.then(function(scanRes) {
        $scope.product.upc = scanRes.text;

        getProduct($scope.product.upc);
    	});

    // apiService.scanBCMock(CommonData)
    //   .then(function(scanRes) {
    //     $scope.product.upc = scanRes.text;
    //
    //     getProduct($scope.product.upc);
    //   });
  };

  $scope.modify = function() {
    apiService.modifyPrice({
      upc: $scope.product.upc,
      price: $scope.product.price
    });
  };

  $scope.add = function(product) {
    apiService.createPrice(product)
      .then(function() {
        $scope.takePicture();
      });
  };

  $scope.takePicture = function() {

    $cordovaCamera.getPicture({
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      correctOrientation: true,
      quality: 100,
      targetWidth: 300,
      targetHeight:300,
      allowEdit: true
    })
    .then(function(imageData) {
        apiService.savePicture({ imageData: imageData, upc: $scope.product.upc })
          .then(function() {
            getProduct($scope.product.upc);
          });
      },
      function(err) {
        console.log("ERROR Camara: " + err);
      }
    );

  };

})

.controller('compareCtrl', function($scope, apiService, CommonData) {
    $scope.product = CommonData.getProduct();
    $scope.products = [];

    apiService.getProductsCompare($scope.product.upc)
      .then(function(res) {
        $scope.products = CommonData.filterMarkets(res);
      });
});
