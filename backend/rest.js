// Application
var bodyParser = require('body-parser');
var express = require('express');
var db = require('./datasource');
var fs = require('fs');
var app = express();

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Pass to next layer of middleware
    next();
});

app.use('/public', express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.text({ limit:1000000 }));

// IMAGEN
app.post('/photo/:upc',function(req, res) {
	var binaryData = new Buffer(req.body, 'base64');
	fs.writeFile(__dirname + '/public/' + req.params.upc +'.jpg', binaryData, function(error) {
		if(error) {
			console.log('[write] Error: ' + err);
		} else {
			console.log('[write] Success: OK');
		}
		res.json({});
	});
});

// SUPERMERCADO
app.get('/supermarkets', function (req, res) {
    db.getSupermarkets(res);
});

app.post('/supermarket', function (req, res) {
    var data = {
      name: req.body.name,
      address: req.body.address
    };

    db.createSupermarket(data, res)
});

app.get('/product/:upc', function (req, res) {
    db.getProductByUpc(req.params.upc, res);
});

// PRECIOS
app.get('/prices/:upc', function (req, res) {
		var data = {
				upc: req.params.upc,
				idSupermarket: null
		};

    db.getPricesByUpc(res, data);
});

app.get('/prices/:upc/:idSupermarket', function (req, res) {
    var data = {
        upc: req.params.upc,
        idSupermarket: req.params.idSupermarket ? req.params.idSupermarket : null
    };

    db.getPricesByUpc(res, data);
});

app.put('/price/:upc/:idSupermarket', function(req, res) {
		var data = {
			upc: req.params.upc,
			idSupermarket: req.params.idSupermarket,
			value: req.body.value
		};

		db.updatePriceByUpcAndIdSupermarket(res, data);
});

app.post('/price', function (req, res) {
    var data = {
      upc: req.body.upc,
      name: req.body.name,
      uom: req.body.uom,
      idSupermarket: req.body.idSupermarket,
      value: req.body.value
    };

    db.createPrice(data, res);
});


app.listen(8003, function () {
    console.log('App listening on port 8003!');
});

exports = module.exports = app;
