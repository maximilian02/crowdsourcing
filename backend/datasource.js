var exports = module.exports = {};
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'crowdsourcing'
});

function openConnection() {
    connection.connect(function (err) {
        if (err) {
          console.log('Error connecting database Crowdsourcing: ', err);
        }
    });
}

// function closeConnection() {
//   connection.end(function (err) {
//     console.log('Closing all connections!');
//     if (err) {
//       console.log('Error closing database connection: ', err);
//     }
//   });
// }

exports.getSupermarkets = function(res) {
    var query = 'SELECT * FROM supermarkets';
    openConnection();
    connection.query(query, function(err, rows, fields) {
        var result = !err ? rows : { result: false, error: err };
        res.json(result);
    });
};

exports.createSupermarket = function(data, res) {
    var query = "INSERT INTO supermarkets (name, address) VALUES ('" + data.name + "', '" + data.address + "')";
    openConnection();
    connection.query(query, function(err, rows, fields) {
        var result = !err ? rows : { result: false, error: err };
        res.json(result);
    });
};

exports.getProductByUpc = function(upc, res) {
    var query = "SELECT * FROM products WHERE upc = '" + upc + "'";
    openConnection();
    connection.query(query, function(err, rows, fields) {
        var result = !err ? rows : { result: false, error: err };
        res.json(result);
    });

};

function searchProductOnMarket(data, callback) {
  var query = "SELECT * FROM prices " +
              "WHERE upc = '" + data.upc + "'" +
              "AND market_id = '" + data.idSupermarket + "'";

  connection.query(query, function(err, rows, fields) {
      var res = !err ? rows : [];
      var result = (res.length ? true : false);

      callback(result);
  });
};

function searchProduct(upc, callback) {
  var query = "SELECT * FROM products WHERE upc = '" + upc + "'";

  connection.query(query, function(err, rows, fields) {
      var res = !err ? rows : [];
      var result = (res.length ? true : false);

      callback(result);
  });
};

exports.updatePriceByUpcAndIdSupermarket = function(res, data) {
    var now = new Date();
    var timestamp = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();

    var queryUpdate = "UPDATE prices SET value = " + data.value + ", date = '" + timestamp +
                "' WHERE upc = '" + data.upc + "' AND market_id = '" + data.idSupermarket + "'";
    var queryInsert = "INSERT INTO prices (upc, market_id, date, value) VALUES ('" + data.upc + "', '" + data.idSupermarket + "', '" + timestamp + "'," + data.value + ")";

    openConnection();

    // If the product price doesn't exists on the market, we need to create it
    // If the product price exist, just update the price
    searchProductOnMarket(data, function(flagExists) {
      var query = flagExists ? queryUpdate : queryInsert;

      connection.query(query, function(err, rows, fields) {
          var result = !err ? rows : { result: false, error: err };
          res.json(result);
      });
    });


};

exports.getPricesByUpc = function(res, data) {
    var query = "SELECT p.upc, p.name as product_name, p.uom, s.name as market_name, s.address, pr.value, pr.date, s.id as market_id FROM prices pr, products p, supermarkets s WHERE pr.upc = p.upc AND pr.market_id = s.id " +
    " AND pr.upc = '" + data.upc + "'" + (data.idSupermarket ? " AND pr.market_id = '" + data.idSupermarket + "'" : '');

    openConnection();
    connection.query(query, function(err, rows, fields) {
        var result = !err ? rows : { result: false, error: err };
        res.json(result);
    });

};

exports.createPrice = function(data, res) {
    var now = new Date();
    var timestamp = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
    var queryProduct = "INSERT INTO products (upc, name, uom) VALUES ('" + data.upc + "', '" + data.name + "', '" + data.uom + "')";
    var queryPrice = "INSERT INTO prices (upc, market_id, date, value) VALUES ('" + data.upc + "', '" + data.idSupermarket + "', '" + timestamp + "'," + data.value + ")";

    openConnection();

    searchProduct(data.upc, function(flagExists) {
        if (!flagExists) {
          connection.query(queryProduct, function(err, rows, fields) {
              var result = !err ? rows : { result: false, error: err };
          });
        }

        connection.query(queryPrice, function(err, rows, fields) {
            var result = !err ? rows : { result: false, error: err };
            res.json(result);
        });

    });

};
